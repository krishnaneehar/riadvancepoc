define(function(){ 
  
  	var _Controller;
  
  	var _actionOnClickLogin = function(){
      
      	var username = _Controller.view.tbxUsername.text;
      	var password = _Controller.view.tbxPassword.text;
      	var previousMilage = _Controller.view.tbxPreviousMilage.text;
      	var startMilage = _Controller.view.tbxStartMilage.text;
      
      	//if(username === null || password === null || previousMilage === null || startMilage === null){
        // 	alert("Please enter valid details.");	
        //}else{
          	var navFrmPropectPipeline = new kony.mvc.Navigation("frmProspectPipeLine");
  			navFrmPropectPipeline.navigate();
        //}
    };
  	
  	var _actionOnClickRecoverPassword = function(){
      	alert("Recover password is not available now.");
    };
  
  	var _bindEvent = function(){
      	_Controller.view.btnLogin.onClick = _actionOnClickLogin;
      	_Controller.view.btnRecoverPassword.onClick = _actionOnClickRecoverPassword;
    };

	var onNavigate = function(){
      	_Controller = this;
      	_bindEvent();
    };
  
	return{
      	onNavigate:onNavigate
    };

 });