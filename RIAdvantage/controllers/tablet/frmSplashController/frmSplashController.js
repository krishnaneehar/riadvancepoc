define(function(){ 
	
  	var loadLoginUI = function(){
      	kony.timer.cancel("NavLogin");
      	var navFrmLogin = new kony.mvc.Navigation("frmLogin");
  		navFrmLogin.navigate();
    };
  
  	var onNavigate = function(){
      	kony.timer.schedule("NavLogin", loadLoginUI, 5, false);
    };
  
	return{
      	onNavigate:onNavigate
    };
 });