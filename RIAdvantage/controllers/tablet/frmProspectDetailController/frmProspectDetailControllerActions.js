define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onSelection defined for lstBxClientType **/
    AS_ListBox_id48a9c3807146b0914d75b4ee3045a5: function AS_ListBox_id48a9c3807146b0914d75b4ee3045a5(eventobject) {
        var self = this;
        this.view.flxAgencyName.setVisibility(true);
    },
    /** onSelection defined for CopylstBxAccountType0d4ca7f89524242 **/
    AS_ListBox_edf87302064b49b98ece22293287e5c6: function AS_ListBox_edf87302064b49b98ece22293287e5c6(eventobject) {
        var self = this;
        this.view.flxOriginDetails.setVisibility(true);
    },
    /** onClick defined for btnSave **/
    AS_Button_c037f0a1c201429abdbe87a4c498cd0b: function AS_Button_c037f0a1c201429abdbe87a4c498cd0b(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmProspectPipeLine");
        ntf.navigate();
    }
});