define(function(){ 
	
  	var _Controller;
  	var isFlexKeyExpand = false;
  
  	var _actionOnClickKey = function(){
      	if(isFlexKeyExpand){
          	isFlexKeyExpand = false;
          	_Controller.view.imgKey.src = "key.png";
          	_Controller.view.flxKeyViewContent.setVisibility(false);
        }else{
          	isFlexKeyExpand = true;
          	_Controller.view.imgKey.src = "arrowcircleright.png";
          	_Controller.view.flxKeyViewContent.setVisibility(true);
        }
    };
  
  	var _actionOnClickNavProspect =function(){
      	var navFrmPropectDetails = new kony.mvc.Navigation("frmProspectDetail");
  		navFrmPropectDetails.navigate();
    };
  
  	var _actionOnRowClickProspect = function(){
      	var selectedData = _Controller.view.segPropsectives.selectedRowItems[0];
      	_Controller.view.lblPropestusTitle.text = selectedData.lblPropesctusName.text;
    };
  
  	var _bindEvent = function(){
      	_Controller.view.flxKey.onClick = _actionOnClickKey;
      	_Controller.view.btnNewProspectus.onClick = _actionOnClickNavProspect;
      	_Controller.view.btnPropectDetails.onClick = _actionOnClickNavProspect;
      	//_Controller.view.segPropsectives.onRowClick = _actionOnRowClickProspect;
    };
 
	var onNavigate = function(){
      	_Controller = this;
      	isFlexKeyExpand = true;
      	_actionOnClickKey();
      	_bindEvent();
      	_Controller.view.segPropsectives.selectedRowIndex = [1,0];
    };
  
	return{
      	onNavigate:onNavigate
    };
 });